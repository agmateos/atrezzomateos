<?php 
$current_options = wp_parse_args(  get_option( 'busiprof_theme_options', array() ), theme_setup_data() );

if( $current_options['home_project_section_enabled'] == 'on' ) { 
?>
<!-- Portfolio Section -->
<!--script>
$("#exampleModalCenterONE").on("show", function () {
  $("body").addClass("modal-open");
}).on("hidden", function () {
  $("body").removeClass("modal-open")
});</script>

<script>
Webflow.push(function() {
  $('.col-md-3').click(function(e) {
    e.preventDefault();
	$('body').css('overflow', 'hidden');
  });

  $('.col-md-3').click(function(e) {
    e.preventDefault();
	$('body').css('overflow', 'auto');
  });
});
</script>-->



<section id="section" class="portfolio bg-color">
	<div class="container">
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<?php if($current_options['protfolio_tag_line']!='') {?>
					<h1 class="section-heading"><?php echo $current_options['protfolio_tag_line']; ?>
					</h1><?php } ?>
					<?php if($current_options['protfolio_description_tag']!='') {?>
					<p><?php echo esc_html($current_options['protfolio_description_tag']); ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- /Section Title -->
		

		<!-- Portfolio Item -->
	<div class="tab-content main-portfolio-section" id="myTabContent">
		<!-- Portfolio Item -->
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModalCenterONE">
					<aside class="post">
						<figure class="post-thumbnail">
							<?php if($current_options['project_one_url']!='') {?>
							
							<?php } ?>
							<?php if($current_options['project_thumb_one']!='') {?>
							<img alt="" src="<?php echo esc_url($current_options['project_thumb_one']); ?>" class="project_feature_img" style="width: 100%" /><!--pruebaclase-->
							<?php } ?></a>
						</figure>
						<div class="portfolio-info">
							<div class="entry-header">
								<?php if($current_options['project_one_url']!='') {?>
								<h4 class="entry-title">
								<?php } if($current_options['project_title_one']!='') {
								echo esc_html($current_options['project_title_one']); ?></a>
								<?php } ?>
								</h4>
							</div>
							<div class="entry-content">
								<?php if($current_options['project_text_one']!='') {?>
								<p><?php echo esc_html($current_options['project_text_one']); ?></p>
								<?php } ?>
							</div>
						</div>					
					</aside>
				</div>
				<div  class="col-md-3 col-sm-6 col-xs-12"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModalCenterTWO">
					<aside class="post">
						<figure class="post-thumbnail">
							<?php if($current_options['project_two_url']!='') {?>							
							<?php } ?>
							<?php if($current_options['project_thumb_two']!='') {?>
							<img alt="" src="<?php echo esc_url($current_options['project_thumb_two']); ?>" class="project_feature_img" style="width: 100%" />
							<?php } ?></a>
						</figure>
						<div class="portfolio-info">
							<div class="entry-header">
								<?php if($current_options['project_two_url']!='') {?>
								<h4 class="entry-title">
								<?php } if($current_options['project_title_two']!='') {
								echo esc_html($current_options['project_title_two']); ?></a>
								<?php } ?>
								</h4>
							</div>
							<div class="entry-content">
								<?php if($current_options['project_text_two']!='') {?>
								<p><?php echo esc_html($current_options['project_text_two']); ?></p>
								<?php } ?>
							</div>
						</div>					
					</aside>
				</div>	
				<div class="col-md-3 col-sm-6 col-xs-12"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModalCenterTHREE">
					<aside class="post">
						<figure class="post-thumbnail">
							<?php if($current_options['project_three_url']!='') {?>							
							<?php } ?>
							<?php if($current_options['project_thumb_three']!='') {?>
							<img alt="" src="<?php echo esc_url($current_options['project_thumb_three']); ?>" class="project_feature_img" style="width: 100%" />
							<?php } ?></a>
						</figure>
						<div class="portfolio-info">
							<div class="entry-header">
								<?php if($current_options['project_three_url']!='') {?>
								<h4 class="entry-title">
								<?php } if($current_options['project_title_three']!='') {
								echo esc_html($current_options['project_title_three']); ?></a>
								<?php } ?>
								</h4>
							</div>
							<div class="entry-content">
								<?php if($current_options['project_text_three']!='') {?>
								<p><?php echo esc_html($current_options['project_text_three']); ?></p>
								<?php } ?>
							</div>
						</div>					
					</aside>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModalCenterFOUR">
					<aside class="post">
						<figure class="post-thumbnail">
							<?php if($current_options['project_four_url']!='') {?>							
							<?php } ?>
							<?php if($current_options['project_thumb_four']!='') {?>
							<img alt="" src="<?php echo esc_url($current_options['project_thumb_four']); ?>" class="project_feature_img" style="width: 100%" />
							<?php } ?></a>
						</figure>
						<div class="portfolio-info">
							<div class="entry-header">
								<?php if($current_options['project_four_url']!='') {?>
								<h4 class="entry-title">
								<?php } if($current_options['project_title_four']!='') {
								echo esc_html($current_options['project_title_four']); ?></a>
								<?php } ?>
								</h4>
							</div>
							<div class="entry-content">
								<?php if($current_options['project_text_four']!='') {?>
								<p><?php echo esc_html($current_options['project_text_four']); ?></p>
								<?php } ?>
							</div>
						</div>					
					</aside>
				</div>
			</div>
	</div>
</section>
<!-- End of Portfolio Section -->
<div class="clearfix"></div>

<!--*****************************************modals******************************************-->
<!-- Modal 1 -->
<div class="modal fade" id="exampleModalCenterONE" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            ...
         </div>
         <aside class="post">
            <figure class="post-thumbnail">
               <?php if($current_options['project_one_url']!='') {?>
               <a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                  <?php } ?>
                  <?php if($current_options['project_thumb_one']!='') {?>
                  <img alt="" src="<?php echo esc_url($current_options['project_thumb_one']); ?>" class="project_feature_img" style="width: 100%" /><!--pruebaclase-->
                  <?php } ?>
               </a>
            </figure>
            <div class="portfolio-info">
               <div class="entry-header">
                  <?php if($current_options['project_one_url']!='') {?>
                  <h4 class="entry-title"><a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                     <?php } if($current_options['project_title_one']!='') {
                        echo esc_html($current_options['project_title_one']); ?></a>
                     <?php } ?>
                  </h4>
               </div>
               <div class="entry-content">
                  <?php if($current_options['project_text_one']!='') {?>
                  <p><?php echo esc_html($current_options['project_text_one']); ?></p>
                  <?php } ?>
               </div>
            </div>
         </aside>
      </div>
   </div>
</div>

<!-- Modal 2 -->
<div class="modal fade" id="exampleModalCenterTWO" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            ...
         </div>
         <aside class="post">
            <figure class="post-thumbnail">
               <?php if($current_options['project_one_url']!='') {?>
               <a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                  <?php } ?>
                  <?php if($current_options['project_thumb_one']!='') {?>
                  <img alt="" src="<?php echo esc_url($current_options['project_thumb_two']); ?>" class="project_feature_img" style="width: 100%" /><!--pruebaclase-->
                  <?php } ?>
               </a>
            </figure>
            <div class="portfolio-info">
               <div class="entry-header">
                  <?php if($current_options['project_one_url']!='') {?>
                  <h4 class="entry-title"><a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                     <?php } if($current_options['project_title_one']!='') {
                        echo esc_html($current_options['project_title_one']); ?></a>
                     <?php } ?>
                  </h4>
               </div>
               <div class="entry-content">
                  <?php if($current_options['project_text_one']!='') {?>
                  <p><?php echo esc_html($current_options['project_text_one']); ?></p>
                  <?php } ?>
               </div>
            </div>
         </aside>
      </div>
   </div>
</div>


<!-- Modal 3 -->
<div class="modal fade" id="exampleModalCenterTHREE" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            ...
         </div>
         <aside class="post">
            <figure class="post-thumbnail">
               <?php if($current_options['project_one_url']!='') {?>
               <a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                  <?php } ?>
                  <?php if($current_options['project_thumb_one']!='') {?>
                  <img alt="" src="<?php echo esc_url($current_options['project_thumb_three']); ?>" class="project_feature_img" style="width: 100%" /><!--pruebaclase-->
                  <?php } ?>
               </a>
            </figure>
            <div class="portfolio-info">
               <div class="entry-header">
                  <?php if($current_options['project_one_url']!='') {?>
                  <h4 class="entry-title"><a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                     <?php } if($current_options['project_title_one']!='') {
                        echo esc_html($current_options['project_title_one']); ?></a>
                     <?php } ?>
                  </h4>
               </div>
               <div class="entry-content">
                  <?php if($current_options['project_text_one']!='') {?>
                  <p><?php echo esc_html($current_options['project_text_one']); ?></p>
                  <?php } ?>
               </div>
            </div>
         </aside>
      </div>
   </div>
</div>


<!-- Modal 4 -->
<div class="modal fade" id="exampleModalCenterFOUR" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            ...
         </div>
         <aside class="post">
            <figure class="post-thumbnail">
               <?php if($current_options['project_one_url']!='') {?>
               <a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                  <?php } ?>
                  <?php if($current_options['project_thumb_one']!='') {?>
                  <img alt="" src="<?php echo esc_url($current_options['project_thumb_four']); ?>" class="project_feature_img" style="width: 100%" /><!--pruebaclase-->
                  <?php } ?>
               </a>
            </figure>
            <div class="portfolio-info">
               <div class="entry-header">
                  <?php if($current_options['project_one_url']!='') {?>
                  <h4 class="entry-title"><a href="<?php echo esc_url($current_options['project_one_url']); ?>">
                     <?php } if($current_options['project_title_one']!='') {
                        echo esc_html($current_options['project_title_one']); ?></a>
                     <?php } ?>
                  </h4>
               </div>
               <div class="entry-content">
                  <?php if($current_options['project_text_one']!='') {?>
                  <p><?php echo esc_html($current_options['project_text_one']); ?></p>
                  <?php } ?>
               </div>
            </div>
         </aside>
      </div>
   </div>
</div>









<?php } ?>