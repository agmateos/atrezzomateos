#WEB ATREZZO MATEOS

##Lanzar docker compose (Arranca nuestro contenedor con la web, ejecutar en la ubicación de docker-compose.yml (raiz del proyecto))

>docker-compose up -d

##Comandos docker varios

###PARAR todos los contenedores Docker (NO USAR SALVO CASOS DE EMERGENCIA, DETIENE TODOS LOS PROYECTOS (con docker))

>docker stop $(docker ps -a -q)

###ELIMINAR todos los contenedores Docker (NO USAR SALVO CASOS DE EMERGENCIA, DETIENE TODOS LOS PROYECTOS (con docker))

>docker rm $(docker ps -a -q)

###listado de todos los contenedores Docker

>docker ps





##UBICACIÓN DE LA WEB EN LOCALHOST (configurable en el docker-compose.yml):

http://localhost:8000/

##Administrador de mi wordpress:

http://localhost:8000/wp-admin/


##LEEME ¡¡¡¡¡IMPORTANTE ANTES DE CADA SUBIDA!!!!!

NUNCA SUBIR AL SERVIDOR WP-CONFIG

SOLO SUBIR WP CONTENT AL SERVICOR